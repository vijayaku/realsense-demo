## Install Realsense SDK

<https://github.com/IntelRealSense/librealsense/releases/download/v2.50.0/Intel.RealSense.SDK-WIN10-2.50.0.3785.exe>

## Create conda environment

```shell
conda create -n py36 python=3.6
```

## activate conda environment

```shell
conda activate py36
```

## Install python libraries

```shell
pip install -r requirements.txt
```

## Connect the Realsense camera  D435i to the usb3 port

## run the demo

```shell
 python .\main.py
```
