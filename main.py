import pyrealsense2 as rs
import cv2
from PIL import Image
import numpy as np

if __name__ == '__main__':
    pipeline = rs.pipeline()
    config = rs.config()    
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
    profile = pipeline.start(config)

    while True:
        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()
        color_image = np.asanyarray(color_frame.get_data())
        print(color_image)

        cv2.namedWindow("Image", cv2.WINDOW_AUTOSIZE)
        cv2.imshow("Image", color_image)
        cv2.waitKey(1)



